-- Update the rental duration and rate of the film
UPDATE film 
SET rental_duration = 21, rental_rate = 9.99
WHERE title = 'Interstellar';


--update customer name and info
UPDATE customer 
SET first_name = 'Bobomurod',
	last_name = 'Iskandarov',
	email = 'freesberg001@gmail.com',
	address_id = 5,
	create_date = current_date
	WHERE customer_id = 10
